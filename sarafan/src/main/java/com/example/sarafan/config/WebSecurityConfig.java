package com.example.sarafan.config;

import com.example.sarafan.domain.User;
import com.example.sarafan.repo.UserDetailsRepo;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.time.LocalDateTime;

@Configuration
@EnableWebSecurity
@EnableOAuth2Sso
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {  //СИСТЕМА ЗАХОДИТ В http И ПЕРЕДАЕТ НА ВХОД ОБЬЕКТ,
        //  В КОТОРОМ МЫ ВКЛЮЧАЕМ: авторизацию, указываем что для этого пути - "/" , мы всем надаем полный доступ
        http
                .antMatcher("/**")
                .authorizeRequests()
                .antMatchers("/", "/login**", "/webjars/**", "/error**") .permitAll()//эти пути доступны для всех(незарегистрированных пользователей)
                .anyRequest() .authenticated()//для всех остальных url-путей требуется авторизация
                .and().logout().logoutSuccessUrl("/").permitAll()
                .and().csrf().disable();
    }

    //ДОБАВЛЯЕМ НАШЕГО АВТОРИЗОВАНОГО ПОЛЬЗОВАТЕЛЯ В БАЗУ ДАННЫХ
    @Bean
    public PrincipalExtractor principalExtractor(UserDetailsRepo userDetailsRepo){
        return map -> {

            String id = (String) map.get("sub");
//СНАЧАЛА ИЩИМ ПОЛЬЗОВАТЕЛЯ В БАЗЕ ДАННЫХ по id, ЕСЛИ НЕ НАЗОДИМ, ТО (orElseGet) - МЫ СОЗДАЕМ НОВОГО ПОЛЬЗОВАТЕЛЯ И С ПОМОЩЬЮ СЕТЕРОВ КЛАДЕМ В НЕГО ВСЮ ИНФУ
            User user = userDetailsRepo.findById(id).orElseGet(()->{
                User newUser = new User();

                newUser.setId(id);
                newUser.setId(id);
                newUser.setName((String) map.get("name"));
                newUser.setEmail((String) map.get("email"));
                newUser.setGender((String) map.get("gender"));
                newUser.setLocale((String) map.get("locale"));
                newUser.setUserpic((String) map.get("picture"));
                //ВОЗВРАЩАЕМ НОВОГО ПОЛЬЗОВАТЕЛЯ, ЕСЛИ У НАС ЕГО НЕТ
                return newUser;
            });
            //УСТАНАВЛИВАЕМ ЕМУ ВРЕМЯ ПОСЛЕДНЕГО ВИЗИТА
            user.setLastVisit(LocalDateTime.now());
            //СОХРАНЯЕМ И ВОЗВРАЩАЕМ НОВОГО ПОЛЬЗОВАТЕЛЯ, ЕСЛИ У НАС ЕГО НЕТ
            return userDetailsRepo.save(user);
        };
    }
}