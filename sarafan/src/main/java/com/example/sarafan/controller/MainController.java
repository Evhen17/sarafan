package com.example.sarafan.controller;

import com.example.sarafan.domain.User;
import com.example.sarafan.repo.MessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

@Controller
@RequestMapping("/")
public class MainController {
    @Autowired
    private MessageRepo messageRepo;
    @GetMapping
    public String main(Model model, @AuthenticationPrincipal User user){    //для получения нашего profile мы будем испотльзовать Spring Security, а именно:  @AuthenticationPrincipal User user
        HashMap<Object,Object> data = new HashMap<>();
// в data мы будем ложить две переменые: profile i message
        data.put("profile", user);
        data.put("messages", messageRepo.findAll());

        model.addAttribute("frontendData", data);
        return "index";
    }
}
