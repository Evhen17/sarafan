package com.example.sarafan.controller;

import com.example.sarafan.domain.Message;
import com.example.sarafan.domain.Views;
import com.example.sarafan.repo.MessageRepo;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    private MessageRepo messageRepo;

    @GetMapping
    @JsonView(Views.IdName.class)
    public List<Message> list(){
        return messageRepo.findAll();
    }

    //Метод который содержит идентификатор - id, для того чтобы по мепингу можно было вызывать поле с данным id
    @GetMapping("{id}")
    @JsonView(Views.FullMessage.class)
    public Message getOne(@PathVariable("id") Message message) {//потому что в путь мы вставляем id
        //для получение записи нужно найти их в списке сообщений
        return message;
    }

    //метод для создания сообщения
    @PostMapping
    public Message create(@RequestBody Message message){
        message.setCreationDate(LocalDateTime.now());
        return messageRepo.save(message);
    }

    //обновление(редактирование) запрса-сообщения
    @PutMapping("{id}")
    public Message update(
            @PathVariable("id") Message messageFromDB,  //это сообщение мы получаем с базы данных
            @RequestBody Message message){  // а это сообщение введено пользователем для редактирования

//спринг нам скопирует все веденное наше сообщение - (message) в браузере, и затем поместит его в базу данных - messageFromDB все поля, кроме -(id)
        BeanUtils.copyProperties(message, messageFromDB, "id");

        return messageRepo.save(messageFromDB);
    }

    //метод удаления сообщения
    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Message message){
        messageRepo.delete(message);
    }
}
